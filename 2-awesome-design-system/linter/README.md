## Linter

List of templates :

| Template name | Description                       |
|---------------|-----------------------------------|
|[.eslint_check](#.typescript_compiler_check)| Execute a lint check using eslint |

### .eslint_check

Execute a lint check using eslint depending on eslint config file.

To customize this job, you can configure the following variables :
 - `ESLINT_CONFIG_FILE_PATH` : Path to the eslint config file (default: .eslintrc)
