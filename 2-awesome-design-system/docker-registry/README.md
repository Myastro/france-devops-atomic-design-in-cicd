## Docker Registry

List of templates :

| Template name | Description |
|---------------|-------------|
|[.DockerRegistry_Image_Publish](#.dockeregistry-image-publish)| Publish a docker image of your application to docker registry |

### .DockerRegistry_Image_Publish

Publish a docker image of your application to docker registry

#### Configurations

- Default `environment` :
    - `name`: "default"

#### Variables

| name | default value | description |
|------|---------------|-------------|
| REGISTRY_TOKEN | $CI_JOB_TOKEN | Token used for authentication with docker registry |
| REGISTRY_URL | $CI_REGISTRY | Url of the docker registry|
| REGISTRY_IMAGE | $CI_REGISTRY_IMAGE | Name of your docker image |
| REGISTRY_IMAGE_TAG | $CI_ENVIRONMENT_NAME | Tag of your docker image |
| BUILD_EXTRAS | '' | Extras argument for docker-build command |
#### Rules

_None_
