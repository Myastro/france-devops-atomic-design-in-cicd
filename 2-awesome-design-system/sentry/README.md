## Sentry

List of templates :

| Template name | Description |
|---------------|-------------|
|[.Sentry_Release_Create](#.sentry-release-create)| Create a Sentry release |

### .Sentry_Release_Create

Create a Sentry release

#### Configurations

- `image`: getsentry/sentry-cli
- Default environment :
    - name : "default'

#### Variables

| name | default value | description |
|------|---------------|-------------|
| SENTRY_URL | $SENTRY_BASE_URL | Url of your sentry app |
| SENTRY_AUTH_TOKEN | $SENTRY_BASE_TOKEN | **Token** used for authentication with sentry app via sentry-cli |
| SENTRY_ORG | $SENTRY_BASE_ORGANIZATION | Name of your Sentry organization |
| RELEASE_VERSION | $CI_COMMIT_SHA | Name of the Sentry release |
| ENVIRONMENT_NAME | $CI_ENVIRONMENT_NAME | Environement name for Sentry release |
| SENTRY_PROJECT | $CI_PROJECT_NAME | Name of the sentry project |

#### Rules
_None_

