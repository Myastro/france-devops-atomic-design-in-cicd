# Shared configuration

Shared configuration auto import some of commons variables used in gitlab-atomic-design-cicd ci/cd devops configuration

If you want to add templates without includes auto include jobs import `main.yml`. If you want to use auto jobs, import `auto-devops.yml`

## Stages
- 🧽 pre-build
- 📦 build
- 🤞 test
- 🐴 pre-deploy
- 🦄 deploy
- 🌈 post-deploy

## Default image

Default image: `docker:latest`

## Auto include jobs

- `Publish branch image on gitlab-registry`

**Description**: Publish docker image on gitlab registry for all featured branch (not develop or staging or master)

**Variables availables**:

| Variable name                               | Description                                                | Default value |
|---------------------------------------------|------------------------------------------------------------|---------------|
| `$REGISTRY_IMAGE_TAG`                         | Image tag of created docker image                          |         $CI_COMMIT_REF_SLUG      |
| `$BUILD_DOCKER_IMAGE_EXTRAS_FOR_BRANCH_IMAGE` | Suffix command that you could pass to `docker build` command |              `''`                    |
