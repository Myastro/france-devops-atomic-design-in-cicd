## Gitlab rules

List of rules :

| Rule name | Description |
|---------------|-------------|
|[.Rules_production](#.rules-production)| Check if is in **production** state |
|[.Rules_pre_production](#.rules-pre-production)| Check if is in **pre production** state |
|[.Rules_staging](#.rules-staging)| Check if is in **staging** state |
|[.Rules_develop](#.rules-develop)| Check if is in **develop** state | 
|[.Rules_MR](#.rules-mr)| Check if is in **merge request** state | 
|[.Rules_OR_production_preproduction_staging_develop_MR](#.rules-OR-production-staging-develop-MR)| Check if is in one of **production**, **pre-production**, **staging**, **develop**, **merge request** state | 

### .Rules_production

Define **production** rules 

#### Rules

If commit tag match a semver X.Y.Z 

Gitlab rule:

    - if: '$CI_COMMIT_TAG =~ /^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)$/'

### .Rules_pre_production

Define **pre-production** rules 

#### Rules

Gitlab rule:

    - if: '$CI_COMMIT_BRANCH == "master"'

### .Rules_staging

Define **staging** rules 

#### Rules

Gitlab rule:

    - if: '$CI_COMMIT_BRANCH == "staging"'

### .Rules_develop

Define **develop** rules 

#### Rules

Gitlab rule:

    - if: '$CI_COMMIT_BRANCH == "develop"'


### .Rules_MR

Define **merge request** rules 

#### Configurations

#### Rules

Gitlab rule:

    - if: '$CI_MERGE_REQUEST_IID'

### .Rules_OR_production_pre_production_staging_develop_MR:

Match one of next rules:
    - production
    - pre-production
    - staging
    - develop
    - MR
