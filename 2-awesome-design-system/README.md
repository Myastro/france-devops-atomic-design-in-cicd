# Gitlab CI/CD Design System

This directory contains a set of CI/CD design system components that you can reuse in your project.

## Docker Registry

All docker registry tools 

## Environments

All gitlab environment defined by gitlab-atomic-design-cicd workflows

## Gitlab-rules

All gitlab rules defined by gitlab-atomic-design-cicd needs

## Sentry

All configuration for sentry usage

## Shared

#### This is pre configured jobs for all atomic-design-in-cicd project.
Just import `shared/main.yml` in your gitlab-ci.yml 
and the magic do the job
