## Environments

List of templates :

| Template name | Description |
|---------------|-------------|
|[.Env_production](#.env-production)| Define **production** environment |
|[.Env_pre_production](#.env-pre-production)| Define **pre-production** environment |
|[.Env_staging](#.env-staging)| Define **staging** environment |
|[.Env_develop](#.env-develop)| Define **develop** environement | 

### .Env_production

Define **production** environment 

#### Configurations

- Default `environment` :
    - `name`: "production'

#### Variables
_None_

#### Rules

If commit tag match a semver X.Y.Z 

Gitlab rule:

    - if: '$CI_COMMIT_TAG =~ /^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)$/'

### .Env_pre_production

Define **pre-production** environment 

#### Configurations

- Default `environment` :
    - `name`: "pre-production'
    
#### Rules

If branch name match 'master' 

Gitlab rule:

    - if: '$CI_COMMIT_BRANCH == "master"'

### .Env_staging

Define **staging** environment 

#### Configurations

- Default `environment` :
    - `name`: "staging'

#### Variables
_None_

#### Rules

If branch name match 'staging' 

Gitlab rule:

    - if: '$CI_COMMIT_BRANCH == "staging"'
    

### .Env_develop

Define **develop** environment 

#### Configurations

- Default `environment`:
    - `name`: "dev'

#### Variables
_None_

#### Rules

If branch name match 'develop' 

Gitlab rule:

    - if: '$CI_COMMIT_BRANCH == "develop"'
